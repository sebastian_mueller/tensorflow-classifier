from datetime import datetime
import os
import math
import re
from keras.layers import Input, Dense, Dropout
from keras.models import Model
import tensorflow as tf
import tensorflow_hub as hub
import datetime
import argparse
from argparse import RawTextHelpFormatter
import textwrap

def step_decay(epoch):
   initial_lrate = learningRate
   drop = 0.5
   epochs_drop = 10.0
   lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
   return lrate


def start_training(model_filename, dataset_file_train, model_name, batchSize, shape, epoch, custom, online, early_stopping, inceptionv3, mobilenetv2, vgg16):
  physical_devices = tf.config.list_physical_devices('GPU')
  print("Num GPUs:", len(physical_devices))

  if custom:
    # Load model
    model = tf.keras.models.load_model('models/custom_models/' + model_name)
    model.summary()
  if online:
    #Load model
    model = tf.keras.Sequential([hub.KerasLayer(model_name, trainable=True)])
    model.build([None, shape, shape, 3])
    model.summary()
  if inceptionv3:
    input_tensor = Input(shape=(shape, shape, 3))
    base_model = tf.keras.applications.inception_v3.InceptionV3(
      include_top=False, weights='imagenet', input_tensor=input_tensor,
      input_shape=(shape, shape, 3), pooling='avg', classes=2,
      classifier_activation='softmax' 
      )
    for layer in base_model.layers:
      layer.trainable = False
    op = Dense(256, activation='relu')(base_model.output)
    op = Dropout(.25)(op)
    output_tensor = Dense(3, activation='softmax')(op)
    model = Model(inputs=input_tensor, outputs=output_tensor)
    model.summary()
  if mobilenetv2:
    input_tensor = Input(shape=(shape, shape, 3))
    base_model = tf.keras.applications.mobilenet_v2.MobileNetV2(
      input_shape=(shape, shape, 3), alpha=1.0, include_top=False, weights='imagenet',
      input_tensor=input_tensor, pooling='avg', classes=3
      )
    for layer in base_model.layers:
      layer.trainable = False
    op = Dense(256, activation='relu')(base_model.output)
    op = Dropout(.25)(op)
    output_tensor = Dense(3, activation='softmax')(op)
    model = Model(inputs=input_tensor, outputs=output_tensor)
    model.summary()
  if vgg16:
    input_tensor = Input(shape=(shape, shape, 3))
    base_model = tf.keras.applications.vgg16.VGG16(
      input_shape=(shape, shape, 3), include_top=False, weights='imagenet',
      input_tensor=input_tensor, pooling='avg', classes=2
      )
    for layer in base_model.layers:
      layer.trainable = False
    op = Dense(256, activation='relu')(base_model.output)
    op = Dropout(.25)(op)
    output_tensor = Dense(2, activation='softmax')(op)
    model = Model(inputs=input_tensor, outputs=output_tensor)
    model.summary()
      
  #Check if dataset_file is a URL and needs to be downloaded or if it is already inside ~/.keras/datasets
  if "http" not in dataset_file_train:
      data_dir=dataset_file_train
      dataset_name = os.path.basename(os.path.normpath(dataset_file_train))
  else:
      #Extract file from path
      origin=dataset_file_train
      idx = dataset_file_train.rfind("/")
      dataset_file_train = dataset_file_train[idx+1:]

      data_dir = tf.keras.utils.get_file(
            fname=dataset_file_train,
            origin=origin,
            extract=True)

      #Extract name from file
      idx = dataset_file_train.rfind(".")
      dataset_name = dataset_file_train[:idx]
      data_dir = os.path.join(os.path.dirname(data_dir), dataset_name)

  optimizer = tf.keras.optimizers.SGD()
 

  #Dataset for training
  print(data_dir)
  train_dataset = tf.keras.preprocessing.image_dataset_from_directory(
      data_dir,
      validation_split=0.2,
      subset="training",
      batch_size=batchSize,
      image_size = (shape, shape),
      seed=123,
      labels='inferred',
      interpolation='bilinear')
  labels = train_dataset.class_names
  print(labels)
  #Dataset for validation
  validation_dataset = tf.keras.preprocessing.image_dataset_from_directory(
      data_dir,
      validation_split=0.2,
      subset="validation",
      batch_size=batchSize,
      image_size = (shape, shape),
      seed=123,
      labels='inferred',
      interpolation='bilinear')

  data_augmentation = tf.keras.Sequential([
    tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal_and_vertical"),
    tf.keras.layers.experimental.preprocessing.RandomRotation(0.2),
  ])
  
  #Apply Data Augmentation to training dataset
  AUTOTUNE = tf.data.AUTOTUNE
  train_dataset = train_dataset.map(lambda x, y: (data_augmentation(x, training=True), y), 
                num_parallel_calls=AUTOTUNE)

  #Configure the dataset for performance
  train_dataset = train_dataset.cache().shuffle(10).prefetch(buffer_size=AUTOTUNE)
  validation_dataset = validation_dataset.cache().prefetch(buffer_size=AUTOTUNE)

  #Normalize data
  normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(1./255)
  train_dataset = train_dataset.map(lambda x, y: (normalization_layer(x), y))
  validation_dataset = validation_dataset.map(lambda x, y: (normalization_layer(x), y))

  #Compile the model with an optimizer and loss function
  model.compile(optimizer=optimizer,
              loss=tf.keras.losses.SparseCategoricalCrossentropy(),
              metrics=['accuracy'])

  log_dir = "models/" + model_filename + "/logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
  callbacks = []
  callbacks.append(tf.keras.callbacks.LearningRateScheduler(step_decay))
  tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
  callbacks.append(tensorboard_callback)
  #Early stopping
  if early_stopping:
      early_stopping_callback = tf.keras.callbacks.EarlyStopping(
            monitor='val_loss', min_delta=0.0001, patience=5, verbose=2,
            mode='min', baseline=None, restore_best_weights=True
            )
      callbacks.append(early_stopping_callback)

  #Train the model
  history = model.fit(
    train_dataset,
    shuffle=True,
    validation_data=validation_dataset,
    epochs=epoch,
    callbacks=callbacks
  )
  
  #Save model and important information
  model.save('models/' + model_filename)
  #convert to tflite model
  converter = tf.lite.TFLiteConverter.from_saved_model('models/' + model_filename)
  tflite_model = converter.convert()
  os.makedirs("models/" + model_filename, exist_ok=True)
  with open('models/' + model_filename + '/' + model_filename, 'wb') as f:
    f.write(tflite_model)
  #Save label file
  os.makedirs('labels/', exist_ok=True)
  labels_file = open("labels/"+dataset_name+".txt", "w")
  for label in labels:
    labels_file.write(label + "\n")
  labels_file.close()
  #Some details concerning the model
  with open("models/" + model_filename +"/details.txt", "w") as file:
    file.write("Model trained with: " + dataset_name + "\n")
    file.write("Training dataset contained " + str(len(train_dataset)) +"samples\n")
    file.write("Models input image shape " + str(shape) +"x" + str(shape))
  os.makedirs("feature_maps/", exist_ok=True)
  #Visualize model
  print('Visualize your model with: python3 -m tensorflow.lite.tools.visualize models/'+model_filename+'/'+model_filename+' models/'+model_filename+'/visualized_model.html')

if __name__ == '__main__':
  parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        usage=textwrap.dedent('''
        With this script you can train a saved model with tensorflow
        '''))
  parser.add_argument(
        '-dt',
        '--dataset_path_train',
        help='dataset used for training'),
  parser.add_argument(
        '-m',
        '--model',
        help='name of the model'),
  parser.add_argument(
        '-e',
        '--epoch',
        type=int,
        help='number of executed epochs during learning')
  parser.add_argument(
        '-lr',
        '--learning_rate',
        type=float,
        help='learning rate used during training')
  parser.add_argument(
        '-b',
        '--batch_size',
        type=int,
        help='batch size')
  parser.add_argument(
        '-s',
        '--shape',
        type=int,
        help='shape of the quadratic image')  
  parser.add_argument(
        '-c',
        '--custom',
        action="store_true",
        default = False,
        help='used in combination with -m. Indicates the use of a custom model')
  parser.add_argument(
        '-es',
        '--early_stopping',
        action="store_true",
        default = False,
        help='flag for using early stopping')
  parser.add_argument(
        '-v3',
        '--inceptionv3',
        action="store_true",
        default = False,
        help='flag for using inbuild InceptionV3')
  parser.add_argument(
        '-mbv2',
        '--mobilenetv2',
        action="store_true",
        default = False,
        help='flag for using inbuild MobileNetV2')
  parser.add_argument(
        '-vgg16',
        '--VGG16',
        action="store_true",
        default = False,
        help='flag for using inbuild VGG16')
  parser.add_argument(
        '-o',
        '--online_tensorflow_hub',
        action="store_true",
        default = False,
        help='used in combination with -m. Indicates the use of a model which gets downloaded from Tensorflow Hub')
  args = parser.parse_args()

  inceptionv3 = args.inceptionv3
  mobilenetv2 = args.mobilenetv2
  vgg16 = args.VGG16
  dataset_path_train = args.dataset_path_train
  shape = args.shape
  learningRate = args.learning_rate
  epoch = args.epoch
  batchSize = args.batch_size
  model_name = args.model
  custom = args.custom
  online = args.online_tensorflow_hub
  early_stopping = args.early_stopping

  #Retrieve number of trained models for use as identifier
  model_number_file = open("model_count.txt","r")
  model_number = int(model_number_file.read())
  if online:
        model_filename = str(model_number) + "_" + str(re.sub('[^0-9a-zA-Z]+', '', model_name)) + "_learningRate=" + str(learningRate) + "_epochs=" + str(epoch) + "_batchSize=" + str(batchSize) + "_earlyStopping= " + str(early_stopping) + ".tflite"
  else:
        model_filename = str(model_number) + "_" + str(model_name) + "_learningRate=" + str(learningRate) + "_epochs=" + str(epoch) + "_batchSize=" + str(batchSize) + "_earlyStopping=" + str(early_stopping) + ".tflite"

  model_number_file = open("model_count.txt","w")
  model_number += 1
  model_number_file.write(str(model_number))
  start_training(model_filename, dataset_path_train, model_name, batchSize, shape, epoch, custom, online, early_stopping, inceptionv3, mobilenetv2, vgg16)


                                                              


