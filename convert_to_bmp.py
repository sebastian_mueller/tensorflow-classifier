import argparse
import glob
from PIL import Image, ImageChops
import textwrap
import os
from argparse import RawTextHelpFormatter

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        usage=textwrap.dedent('''
        Execute this script to convert all images inside '/images' to .bmp. Add '-n' to script call if you also want to negate the images. The converted images are stored inside /images/bmp
        ''')
    )
    parser.add_argument(
        '-n',
        '--negate',
        default=False,
        action="store_true",
        help='negate passed image')
    args = parser.parse_args()

    for file in glob.glob('images/*.*'): 
        img=Image.open(file)
        filename = os.path.basename(os.path.normpath(file)).split('.')[0]
        img.save( 'images/bmp/'+ filename +'.bmp', 'bmp')

        if (args.negate == True):
            inv_img = ImageChops.invert(img)
            inv_img.save( 'images/bmp/'+ filename +'-negate.bmp', 'bmp')