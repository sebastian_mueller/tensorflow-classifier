import netron
import argparse
from argparse import RawTextHelpFormatter
import textwrap

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        usage=textwrap.dedent('''
        With this script you can visualize a tflite model with netron.
        '''))
    parser.add_argument(
        '-m',
        '--model',
        help='name of the model')
    args = parser.parse_args()
    model = args.model
    model_base_path='models/'
    model_path=model_base_path + model + '/' + model
    netron.start(model_path)