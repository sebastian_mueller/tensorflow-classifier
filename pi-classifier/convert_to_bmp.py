import argparse
import glob
from PIL import Image

def convert():
    for file in glob.glob('images/*.*'): 
        img=Image.open(file)
        filename = file.split('/')[1].split('.')[0]
        img.save( 'images/bmp/'+ filename +'.bmp', 'bmp')