# Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""label_image for tflite."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import time
import RPi.GPIO as GPIO

import numpy as np
from PIL import Image
import tflite_runtime.interpreter as tflite

import display as lcd
import camera as pi_v2
import convert_to_bmp as bmp_converter


def load_labels(filename):
  with open(filename, 'r') as f:
    return [line.strip() for line in f.readlines()]

def start_classification(model_file, num_threads, input_mean, input_std, label_file):
  lcd.print_output("Processing", "...", lcd_driver)
  time.sleep(3)
  interpreter = tflite.Interpreter(
      model_path="models/"+model_file+"/"+model_file, num_threads=num_threads)
  interpreter.allocate_tensors()

  #Information about the input/output tensor are returned. Return type is an dictionary with following fields:
    #'name': The tensor name.
    #'index': The tensor index in the interpreter.
    #'shape': The shape of the tensor.
    #'quantization': Deprecated, use 'quantization_parameters'. This field only works for per-tensor quantization, whereas
    #'quantization_parameters' works in all cases.
    #'quantization_parameters': The parameters used to quantize the tensor:
      #'scales': List of scales (one if per-tensor quantization)
      #'zero_points': List of zero_points (one if per-tensor quantization)
      #'quantized_dimension': Specifies the dimension of per-axis quantization, in the case of multiple scales/zero_points.

  input_details = interpreter.get_input_details()
  output_details = interpreter.get_output_details()

  # check the type of the input tensor
  floating_model = input_details[0]['dtype'] == np.float32

  # NxHxWxC, H:1, W:2
  height = input_details[0]['shape'][1]
  width = input_details[0]['shape'][2]

  #Resize image used for classification in order to match the tensor input size
  img = Image.open("images/bmp/image.bmp").resize((width, height))

  # img got the shape [224,224,3]. Add N dim to image in order to match the input tensor shape [1,224,224,3]
  input_data = np.expand_dims(img, axis=0)

  if floating_model:
    input_data = (np.float32(input_data) - input_mean) / input_std

  interpreter.set_tensor(input_details[0]['index'], input_data)

  start_time = time.time()
  #Invoke inference
  interpreter.invoke()
  stop_time = time.time()

  #Get output tensor
  output_data = interpreter.get_tensor(output_details[0]['index'])
  results = np.squeeze(output_data)
  top_k = results.argsort()[-5:][::-1]
  labels = load_labels("labels/"+label_file)
  for i in top_k:
    if floating_model:
      print('{:08.6f}: {}'.format(float(results[i]), labels[i]))
    else:
      print('{:08.6f}: {}'.format(float(results[i] / 255.0), labels[i]))

  print('time: {:.3f}ms'.format((stop_time - start_time) * 1000))

  #Print to lcd display
  if floating_model:
    lcd.print_output(labels[top_k[0]], '{:08.6f}'.format((float(results[top_k[0]]))) + '%', lcd_driver)
  else: 
    lcd.print_output(labels[top_k[0]], '{:08.6f}'.format((float(results[top_k[0]])) / 255.0 ) + '%', lcd_driver)

  time.sleep(3)

  #Append Input and output details of the tensor to the details file
  with open("models/" + model_file +"/details.txt", "a") as file:
    file.write("\n\nTensor input details: " + str(input_details))
    file.write("\n\nTensor output details: " + str(output_details))
  lcd.print_output("waiting for", "picture", lcd_driver)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '-m',
      '--model_file',
      help='.tflite model to be executed')
  parser.add_argument(
      '-l',
      '--label_file',
      default="labels.txt",
      help='name of file containing labels')
  parser.add_argument(
      '--input_mean',
      default=127.5, type=float,
      help='input_mean')
  parser.add_argument(
      '--input_std',
      default=127.5, type=float,
      help='input standard deviation')
  parser.add_argument(
      '--num_threads', default=None, type=int, help='number of threads')
  args = parser.parse_args()

  model_file = args.model_file
  num_threads = args.num_threads
  input_mean = args.input_mean
  input_std = args.input_std
  label_file = args.label_file
  lcd_driver = lcd.get_driver() 
  #Setup GPIO 12 for button input
  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BOARD)
  GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

  lcd.print_output("waiting for", "picture", lcd_driver)
  while True:
    if GPIO.input(12) == GPIO.HIGH:
      lcd.print_output("Taking picture", "...", lcd_driver)
      time.sleep(3)
      pi_v2.take_picture()
      bmp_converter.convert()
      start_classification(model_file, num_threads, input_mean, input_std, label_file)
  

