from picamera import PiCamera
from time import sleep

def take_picture():
    camera = PiCamera()
    camera.capture('images/image.jpg')
    camera.stop_preview()
    camera.close()
