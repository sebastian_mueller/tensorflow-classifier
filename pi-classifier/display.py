import lcddriver
from time import *

def get_driver():
    lcd = lcddriver.lcd()
    return lcd

def print_output(line1, line2, lcd):
    lcd.lcd_clear()
    lcd.lcd_display_string(line1, 1)
    lcd.lcd_display_string(line2, 2)
