## Installationsprozess von TensorFlow ##

Dieser Installationsprozess wird auf dem Gerät durchgeführt, welches Modelle trainieren soll. Gehe also sicher, dass genug Ressourcen in Form von Hardware zur Verfügung stehen. Folge dazu der Anleitung oder führe folgendes Skript aus `tensorflow-classifier/setup.sh`

0. Update packages

    * `sudo apt-get update`

    * `sudo apt-get upgrade`
    
    `Python3.6.9` sollte installiert sein. Für die `tensorflow-classifier` Skripte wird `Python3.7` benötigt. `Python3.7` sollte aber unter keinen Umständen direkt im System installiert und verwendet werden, da hierdurch das OS schwerwiegend beeinflusst werden kann. Aus diesem Grund wird eine virtuelle Umgebung für `Python3.7` erzeugt mit allen nötigen Modulen.

1. Installation von `python3.7` in einer venv (virtuellen Umgebung) und Aktivierung der venv

    * `sudo apt-get install python3.7 python3-venv python3.7-venv`

    * `python3.7 -m venv --system-site-packages py37-venv`

    * `. py37-venv/bin/activate`

    * `python3 --version`

2. Installation von `pip3` (Paket- Modulmanager für python3)

    * `sudo apt update`

    * `sudo apt install python3-pip`

    * `sudo -H pip3 install --upgrade pip`

    * `python3 -m pip install --upgrade pip`

    * `pip3 --version`

3. Installation von `tensorflow 2.5`

    * `sudo apt-get install python3-testresources`

    * `pip3 install tensorflow==2.5`

    * `pip3 install jupyter`

    * `pip3 install matplotlib`

    * `sudo apt-get install pyqt5-dev-tools`

    * `pip3 install pyqt5 lxml`

4. Installation von `tensorflow lite` für python

    * `echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list`

    * `curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -`

    * `sudo apt-get update`

    * `sudo apt-get install python3-tflite-runtime`

    Installation von `pillow anstatt` `PIL`. `Pillow` ist ein fork von `PIL`, da `PIL` seit 2009 nicht mehr unterstüzt und gewartet wird.

	* `python3 -m pip install --upgrade Pillow`

    * `sudo apt-get install libjbig0 liblcms2-2 libopenjp2-7 libtiff5 libwebp6 libwebpdemux2 libwebpmux3`

5. Installation von `tensorflow model maker`

    Globale Installation der header files und static libraries für python dev auf dem lokalen System

    * `sudo apt-get install python3.7-dev`

    Upgrade `setuptools wheel` 

    * `pip3 install --upgrade pip setuptools wheel`

    * `pip3 install --upgrade pip`

    Installation von `opencv-python`

    * `pip3 install opencv-python`

    Installation der vorrausgesetzten Pakete für `tensorflow model maker`

    * `pip3 install tf-models-official tensorflow-hub numpy pillow sentencepiece fire absl-py urllib3 tflite-support tensorflowjs tensorflow librosa lxml PyYAML matplotlib six tensorflow-addons neural-structured-learning tensorflow-model-optimization Cython`

    Installation von `tflite model maker`

    * `pip3 install tflite-model-maker`

5. Installation von `netron` für Visualisierungen

    * `pip3 install netron`

6. Installation von `CUDA` zur Programmbeschleunigung mit Nvidia GPUs
    
    * [Ubuntu 18.04](https://gist.github.com/eddex/707f9cbadfaec9d419a5dfbcc2042611)
    * [Windows](https://www.codingforentrepreneurs.com/blog/install-tensorflow-gpu-windows-cuda-cudnn/) 

