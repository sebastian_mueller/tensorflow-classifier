# Skripte #

### Skripte im Verzeichnis tensorflow-classifier/pi-classifier/ ###
Alle Dateien innerhalb des Verzeichnis `pi-classifier` werden nur auf dem Raspberry Pi verwendet. Zu diesen gehören folgende:

1. `camera.py` und `display.py` wird als Modul innerhalb von `label_image_pi.py` verwendet

2. `i2c_lib.py` und `lcddriver.py` werden benötigt, um das LCD Display verwenden zu können

3. `convert_to_bmp.py` wird als Modul in `label_image_pi.py` verwendet, um Bilder zu `.bmp` zu konvertieren. Dieser Dateityp wird von TensorFlow erwartet.

4. `label_image_pi.py`:

        Dieses Skript startet den Prototypen. Es läuft eine Dauerschleif in welcher der Prozess zur Klassifizierung gestartet wird, wenn mit der Schaltfläche des Prototypen die Raspberry Pi Kamera ausgelöst wird. Das aufgenommene Bild wird anschließend klassifiziert und das LCD Display gibt Auskunft über den Stand der Anwendung.

        Beim Starten der Anwendung sind zwei Optionen anzugeben.

        -m / --model_file
       
        -l / --label_file

        Für -m wird nur der Name des (.tflite) Modells erwartet, welches verwendet werden soll. Dieses muss sich in pi-classifier/models/ in einem Verzeichnis mit dem selben Namen befinden. Also: pi-classifier/models/<model_name>/<model_name>.tflite

        Für -l wird nur der Name der (.txt) Datei erwartet, welche die zu klassifizierenden Labels enthält. Diese Datei muss sich innerhalb von pi-classifier/labels/ befinden.

        Der Aufruf des Skriptes sieht wie folgt aus:

        python3 label_image_pi.py -m <model_name> -l <label_file_name>

        Wichtig ist, dass die venv für Python 3 aktiviert ist.

5. `requirements.txt`enthält eine Liste zu installierender Python Module. Diese wird in `setup.sh` verwendet

6. `setup.sh` richtet den Raspberry Pi mit der notwendigen Ordnerstruktur und der venv für Python3 ein. Gestartet wird die venv innerhalb von `pi-classifier/` mit `. py37-venv/bin/activate`

### Skripte im Verzeichnis tensorflow-classifier/ ###

1. `convert_to_bmp.py`:

        Mit diesem Skript werden ALLE Bilder im Verzeichnis tensorflow-classifier/images/ zu .bmp Dateien konvertiert und im Verzeichnis tensorflow-classifier/images/bmp/ gespeichert. Zusätzlich is es möglich mit dem setzen der Option -n die konvertierten Bilder zusätzlich zu negieren. 

2. `netron_visualizer.py`:

        Mit diesem Skript lassen sich tflite Modelle visualisieren. Lediglich der Name des Modells muss der Option -m / --model übergeben werden. Eine Alternative ist tensorboard. Ausgeführt wird tensorboard wie folgt:
        
        tensorboard --logdir tensorflow-classifier/models/<model_name>/logs/<log_folder>

3. `resize_img.py`:

        Mit diesem Skript ist es möglich Bilder in einem Ordner neu nach unten zu skalieren und dabei die Verhältnisse bzgl Breite und Höhe beizubehalten. Dadurch ist es leichter bei Bedarf ganze Datensätze zu bearbeiten. In manchen Fällen sind Bilder zu groß um sie zum Lernen zu verwenden. Grund dafür wird die limitiert zur Verfügung stehende Hardware sein. Dies wäre ein Anwednungsfall für dieses Skript. Notwendig sind dabei drei Optionen.

        -sp / --path_source gibt den Pfad zu den Bildern an, welche skaliert werden sollen
        
        -dp / --path_destination gibt den Speicherort für die neu skalierten Bilder an 
        
        -sf / --scale_factor gibt an um welchen Faktor die Bilder nach unten skaliert werden sollen

        WICHTIG: Das Skript skaliert NUR nach UNTEN und nicht nach oben

4. `visualize_feature_maps.py`:

        Dieses Skript visualisiert die Aktivierungsmatrizen eines Bildes aller faltender Schichten eines CNN. Insgesamt müssen drei Optionen angegeben werden.

        -m / --model der Modellname von welchem Aktivierungsmatrizen visualisiert werden sollen. Verwendet wird vom Skript die saved_model.pb Datei und nicht die .tflite Datei des Modells.

        -i / --image das Bild zu welchem visualisierte Aktivierungsmatrizen erzeugt werden sollen.

        -a / --architecture die Architektur auf welcher --model basiert. Hier werden aktuell nur die Architekturen VGG-16, InceptionV3 und MobileNetV2 unterstützt.

        Gespeichert werden die Visualisierungen in tensorflow-classifier/models/<model>/feature_maps/

5. `label_image.py`:

        Dieses Skript kann dafür verwendet werden trainierte Modelle anzutesten bevor man sie auf dem Raspberry Pi ausführt. Umgesetzt wird das selbe wie bei label_image_pi.py. Der Unterschied ist, dass keine Dauerschleife existiert und das Bild beim Start des Skripts selbst mit angegeben werden muss. Demenstprechend wird die Ausführung des Programmes nach der Klassifizierung beendet. Notwendig sind drei Optionen.

        -m / --model_file
       
        -l / --label_file

        -i / --image

        Für -m wird nur der Name des (.tflite) Modells erwartet, welches verwendet werden soll. Dieses muss sich in tensorflow-classifier/models/ in einem Verzeichnis mit dem selben Namen befinden. Also: tensorflow-classifier/models/<model_name>/<model_name>.tflite

        Für -l wird nur der Name der (.txt) Datei erwartet, welche die zu klassifizierenden Labels enthält. Diese Datei muss sich innerhalb von tensorflow-classifier/labels/ befinden.

        Für -i wird der gesamte Pfad zum Bild benötigt.

6. `train_model.py`:

        Mit diesem Skript werden Modelle trainiert. Möglich sind dabei folgende Optionen: 

        -dt / --dataset_path_train

        -m / --model

        -e / --epoch

        -lr / --learning_rate

        -b / --batch_size

        -s / --shape

        -es / --early-stopping

        -v3 / --inceptionv3

        -mbv2 / --mobilenetv2

        -vgg16 / --VGG16

        Mit -dt wird der Datensatz angegeben, welcher zum Trainieren des Modells verwendet wird. Angegeben wird der absolute Pfad.

        Mit -m gibt man einen Teil des resultierenden Modellnamen an. Bspw: mein_modell
        Der Modellname wird zusammen mit anderen Parametern wie der Lernrate zussamengesetzt.

        Die Option -e gibt an nach wie vielen Epochen spätestens mit dem Training aufgehört werden soll.

        Mit -lr wird die initial verwendete Lernrate angegeben. Diese wird während dem Training dynamisch angepasst.

        Die Option -b definiert die Menge an Trainingsbeispielen für die eine Änderung an den Gewichtsvektoren vorgenommen wird.

        Mit -s wird die shape des Eingabetensors bestimmt.

        Setzt man die Option -es wird vorzeitiges Stoppen im Training verwendet. Dieses tritt ein, wenn nach 5 Epochen keine Fortschritte erzielt wurden. Ein Fortschritt entspricht aktuell einer fortschrittlichen Änderung der 4ten Stelle des val_loss Wertes. Lässt man die Option -es aus wird das vorzeitige Stoppen nicht verwendet.

        Mit den Optionen -v3, -mbv2 und vgg16 gibt man an, welche CNN Architektur verwendet werden soll. 

        Ein korrekter Aufruf des Skriptes sieht wie folgt aus:

        python .\train_model.py -dt A:\masks2Resized4_3classes -e 500 -lr 0.001 -b 64 -v3 -s 224 -m inceptionV3 -es

        Das daraus resultierende Modell hätte folgenden Namen:
        <unique_id>_inceptionV3_learningRate=0.001_epochs=500_batchSize=64_earlyStopping=True.tflite

        Mit dem Ende des Trainings wird die dazugehörige label Datei in tensorflow-classifier/labels/ als .txt Datei gespeichert. Des Weiteren wird im Verzeichnis des Modells (tensorflow-classifier/models/<model_name>) aus dem saved_model.pb die .tflite Datei erzeugt.

6. `setup.sh` richtet den Rechner mit der venv für Python3 ein. Gestartet wird die venv innerhalb von `pi-classifier/` mit `. py37-venv/bin/activate`.