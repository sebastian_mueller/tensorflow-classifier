## Installationsprozess der Umgebung des Prototypen auf RaspBian ##

<b>WICHTIG:  Ohne die Hardware in Form der Raspberry Pi Kamera V2, der Schaltfläche (Knopf) und dem LCD Display wird das Programm des Prototypen scheitern.</b>

1. Lade das OS [image](https://www.raspberrypi.org/software/operating-systems/) herunter

2. Vorbereitung der SD Karte

    * `lsblk -p`

    Unmount Partitionen auf der SD Karte
    
    * `umount <partition>`

    Kopiere das Image auf die SD Karte

    * `sudo dd if=<Path/to/image> of=<Partition/SDcard> bs=4M conv=fsync`

    Füge eine leere Datei mit dem Namen `ssh` der boot Partition hinzu. Der Raspberry Pi wird dadurch autmatisch SSH mit den folgenden Anmeldedaten einrichten.
    * `User: pi`
    * `Password: raspbian`

3. Die SD Karte einstecken und den Pi starten. Per `ssh` mit dem Raspberry Pi verbinden. 

4. Als nächstes den Raspberry Pi mit den notwendigen Srkipten für das Projekt einrichten  via `scp` und `ssh`.


    * Klone folgendes git [repo](https://gitlab.com/sebastian_mueller/tensorflow-classifier) auf den Rechner welcher sich mit dem Pi via ssh verbinden soll

    * Kopiere mit `scp` das `pi-classifier` Verzeichnis auf den Raspberry Pi

    * Führe das Skript `setup.sh` innerhalb des `pi-classifier` Verzeichnis aus. Dieses Skript erzeugt eine virtuelle Umgebung für Python3 mit allen benötigten Modulen und der notwendigen Ordnerstruktur für das laufende Programm des Prototypen.