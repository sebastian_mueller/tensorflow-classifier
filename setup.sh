#!/bin/bash
sudo apt-get update
sudo apt-get upgrade

#Install python3.7 via venv and activate it
sudo apt-get install python3.7 python3-venv python3.7-venv
python3.7 -m venv --system-site-packages py37-venv
. py37-venv/bin/activate
python3 --version

#Install pip3 for python3
sudo apt update
sudo apt install python3-pip
sudo -H pip3 install --upgrade pip
python3 -m pip install --upgrade pip
pip3 --version

sudo apt-get install python3-testresources
sudo apt-get install pyqt5-dev-tools

#Install tensorflow lite for python
echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
sudo apt-get install curl
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install python3-tflite-runtime
#Install pillow instead of PIL. Pillow is a fork of PIL but PIL isn't supported since 2009.
python3 -m pip install --upgrade Pillow
sudo apt-get install libjbig0 liblcms2-2 libopenjp2-7 libtiff5 libwebp6 libwebpdemux2 libwebpmux3

#Install header files and static libraries for python dev system wide
sudo apt-get install python3.7-dev
#Upgrade setuptools wheel
pip3 install --upgrade pip setuptools wheel
pip3 install --upgrade pip
#Install opencv-python
pip3 install opencv-python==4.5.2.54

#Install requirements
pip3 install -r requirements.txt
