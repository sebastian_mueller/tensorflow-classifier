from PIL import Image
import os
import argparse
import PIL
import glob
import textwrap
from argparse import RawTextHelpFormatter
from subprocess import check_output

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        usage=textwrap.dedent('''
        Execute this script to scale down images by a user defined factor
        ''')
    )
    parser.add_argument(
        '-sf',
        '--scale_factor',
        type=int,
        default=4,
        help='factor of downscaling')
    parser.add_argument(
        '-sp',
        '--path_source',
        help='path of the images to be resized')
    parser.add_argument(
        '-dp',
        '--path_destination',
        help='path of the resized images')
    args = parser.parse_args()

    scale_factor = args.scale_factor
    path_source = args.path_source
    path_destination = args.path_destination
    
    number_images_in_source_path = len(os.listdir(path_source))-1 #-1 because of the resized folder
    resized_images_counter = 0
    for file in glob.glob(path_source+ '*.*'): 
        resized_images_counter += 1
        print("resized/total images: " + str(resized_images_counter) + "/" + str(number_images_in_source_path), end='\r')
        try:
            image = Image.open(file)
            width, height = image.size
            filename = os.path.basename(os.path.normpath(file)).split('.')[0]
            resized_image = image.resize((int(width/scale_factor),int(height/scale_factor)))
            resized_image.save(path_destination + filename + '.jpg')
        except (PIL.UnidentifiedImageError, OSError):
            print("UnidentifiedImageError: " + filename)