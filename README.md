# Anleitung zur Verwendung eines prototypischen mobilen Detektors # 

Alle entsprechenden Anleitungen zur Ausführung dieses Projektes befinden sich im Verzeichnis `docs/`. Beginnend mit `0_` sind die Dokumentationen in der empfohlenen Reihenfolge gekennzeichnet. In `skript_documentation.md` werden alle vorhanden Skripte erklärt.

Grundvorraussetzung ist folgende Hardware:

* Raspberry Pi 4 Modell B
* I2C Schnittstelle
* I2C Logik Level Konverter
* LCD Modul HD44780 16x2
* Eine Schaltfläche ( ein Knopf als Auslöser der Kamera)
* Raspberry Pi Kamera V2
* Jumper Kabel
* Ein Steckbrett
* Einen Widerstand

Das unten gezeigte Bild veranschaulicht die Verbindungen des Prototypen. Diese Sollte identisch umgesetzt werden. Bei unterschiedlicher Belegung des GPIO für die Schaltfläche muss eine Anpassungen in `label_image_pi.py` vorgenommen werden. Wenn eine falsche Belegung der Pins für die Erdung oder Stromversorgung vorliegt, ist es möglich dass der Raspberry Pi beschädig wird. Die Erdung wird im Schaltplan mit schwarzen und die Stromversorgung mit roten Verbindungen dargestellt. Genauere Informationen zum gesamten Projekt sind ersichtlich in der Master Thesis `Entwicklung und Aufbau eines prototypischen mobilen Detektors mit Hilfe von Bilderkennung`, welche auf dieser [Seite](https://www.christianbaun.de/) einsehbar ist. 

![Schaltplan des Prototypen](docs/images/Schaltplan.png)

Wurde alles erfolgreich eingerichtet und Modelle trainiert welche nun auf dem Raspberry Pi eingesetzt werden sollen, müssen das Modell und die zugehörige label Datei per `scp` auf den Raspberry Pi in die entsprechenden Verzeichnisse kopiert werden. Genauere Informationen dazu stehen in `skript_documentation.md` unter dem Punkt zu `label_image_pi.py`.