import argparse
from argparse import RawTextHelpFormatter
import textwrap
import tensorflow as tf
from keras.models import Model
from matplotlib import pyplot
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.mobilenet_v2 import preprocess_input
from numpy import expand_dims

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=RawTextHelpFormatter,
        usage=textwrap.dedent('''
        With this script you can train a saved model with tensorflow
        '''))
    parser.add_argument(
        '-m',
        '--model',
        required=True,
        help='name of the model'),
    parser.add_argument(
      '-i',
      '--image',
      required=True,
      help='image to be classified')
    parser.add_argument(
      '-a',
      '--architecture',
      required=True,
      help='model architecture')

    args = parser.parse_args()
    model_name = args.model
    image = args.image
    architecture = args.architecture

    #Array with strings which identify an conv layer
    conv_layer_string_id = []
    #Array with strings who should not be contained
    conv_layer_string_id_without = []
    if architecture == "mobilenetv2":
        conv_layer_string_id.append('conv')
        conv_layer_string_id.append('Conv')
        conv_layer_string_id.append('Conv_')
        conv_layer_string_id.append('project')
        conv_layer_string_id.append('depthwise')
    if architecture == "vgg16":
        conv_layer_string_id.append('conv')
    if architecture == "inceptionv3":
        conv_layer_string_id.append('conv')

    model_base = tf.keras.models.load_model('models/' + model_name)
    model_base.summary()
    conv_indexes = []
    conv_channels = []

    for i in range(len(model_base.layers)):
        layer = model_base.layers[i]
        # check for convolutional layer
        if any(conv_identifier in layer.name for conv_identifier in conv_layer_string_id):
            
            if len(conv_layer_string_id_without) > 0:
                if any(conv_identifier_forbidden not in layer.name for conv_identifier_forbidden in conv_layer_string_id_without):
                    conv_indexes.append(i)
                    conv_channels.append( layer.output.shape[3])
                    print(i, layer.name, layer.output.shape)
            else:
                # summarize output shape
                conv_indexes.append(i)
                conv_channels.append( layer.output.shape[3])
                print(i, layer.name, layer.output.shape)
    
    conv_block = 1
    for conv_layer in range(len(conv_indexes)):
        index = conv_indexes[conv_layer]
        # redefine model to output right after the first hidden layer
        model = Model(inputs=model_base.inputs, outputs=model_base.layers[index].output)
        img = load_img(image, target_size=(224, 224))
        # convert the image to an array
        img = img_to_array(img)
        # expand dimensions so that it represents a single 'sample'
        img = expand_dims(img, axis=0)
        # prepare the image (e.g. scale pixel values for the vgg)
        img = preprocess_input(img)
        # get feature map for first hidden layer
        feature_maps = model.predict(img)

        # plot all 32 maps in an 8x4 squares
        square = int(conv_channels[conv_layer]/4)
        ix = 1
        for _ in range(square):
            for _ in range(4):
                # specify subplot and turn of axis
                ax = pyplot.subplot(square, square, ix)
                ax.set_xticks([])
                ax.set_yticks([])
                # plot filter channel in grayscale
                pyplot.imshow(feature_maps[0, :, :, ix-1], cmap='gray')
                ix += 1
        # show the figure
        pyplot.savefig("models/" + model_name + "/feature_maps/" + str(conv_block) + "_" + str(model_base.layers[index].name) + ".jpg")
        conv_block += 1